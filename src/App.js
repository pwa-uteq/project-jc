import React from "react";
import { AddTask } from "../src/components/AddTask";
import { MainContent } from "../src/components/MainContent";
import { Sidebar } from "../src/components/Sidebar";
import { Table } from "../src/components/To-DoTable/Table";
import { Topbar } from "../src/components/Topbar";
import { Banner } from "../src/components/Banner";
import { Button } from "./components/Button";
import { useDispatch } from "react-redux";
import { clearTasks } from "./store/slices/rootSlice";
import "./styles/styles.scss";

function App() {
  const dispatch = useDispatch();
  return (
    <>
      <Sidebar />
      <Topbar title="Home" />
      <MainContent>
        <Banner />
        <Table />
        <Button
          onClick={() => dispatch(clearTasks())}
          text={"Reset counter"}
          className={"button-reset"}
        />
      </MainContent>
      <AddTask />
    </>
  );
}

export default App;
