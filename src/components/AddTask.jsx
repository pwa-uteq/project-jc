import { XMarkIcon } from "@heroicons/react/24/solid";
import { useState } from "react";
import { Button } from "./Button";
import { nanoid } from "nanoid";
import { useDispatch, useSelector } from "react-redux";
import { addTask, setIsFormOpen } from "../store/slices/rootSlice";

export const AddTask = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.isFormOpen);
  const [value, setValue] = useState("");
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleSubmit = (event) => {
    const task = {
      id: nanoid(6),
      title: value,
      isDone: false,
      favorite: false,
    };
    setValue("");
    dispatch(addTask(task));
    dispatch(setIsFormOpen(false));
  };

  return (
    <div
      className={`${isOpen ? "flex" : "hidden"} add-task-container justify-end`}
    >
      <div className="add-task rounded-t-lg" id="add-task">
        <div className="container">
          <div className="header p-4 flex justify-between">
            <span className="small-semibold">New task</span>
            <XMarkIcon
              onClick={() => dispatch(setIsFormOpen(false))}
              className="w-4 cursor-pointer"
            />
          </div>
          <div className="body p-4">
            <input
              value={value}
              onChange={handleChange}
              className="input-task"
              placeholder="Add your task"
              type="text"
            />
          </div>
          <div className="footer p-4 flex justify-end">
            <Button
              onClick={handleSubmit}
              className={"ghost"}
              text={"Create task"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
