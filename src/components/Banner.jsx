import React from "react";
import ProgressBar from "./ProgressBar";

export const Banner = () => {
  return (
    <div className="banner flex justify-center pt-8 pb-12">
      <div className="banner-content w-96">
        <p className="smaller-semibold text-center pb-1">Today, Octuber 11</p>
        <h3 className="h3 text-center pb-8">Good evening, Josh</h3>
        <ProgressBar />
      </div>
    </div>
  );
};
