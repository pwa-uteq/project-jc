import React from "react";

export const Button = ({ text, className, onClick = () => {} }) => {
  return (
    <a onClick={onClick} className={`${className} button smaller`} href="#">
      {text}
    </a>
  );
};
