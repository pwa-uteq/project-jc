import React from "react";

export const MainContent = ({ children }) => {
  return (
    <section className="section-main">
      <main className="main">{children}</main>
    </section>
  );
};
