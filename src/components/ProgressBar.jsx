import React from "react";
import { useSelector } from "react-redux";

const ProgressBar = () => {
  const tasks = useSelector((state) => state.tasks);
  return (
    <div className="progressbar px-8 pt-3 pb-5 rounded-full">
      <div className="content">
        <div className="progress-text w-full flex justify-between pb-2">
          <p className="text small">Progress</p>
          <div className="steps small">
            <span id="done" className="done ">
              {tasks.filter((task) => task.isDone).length}
            </span>
            /
            <span id="pending" className="pending">
              {tasks.length}
            </span>
          </div>
        </div>
        <div className="bar h-2 w-full rounded-full"></div>
      </div>
    </div>
  );
};

export default ProgressBar;
