import {
  Bars3Icon,
  HomeIcon,
  SunIcon,
  StarIcon,
  UserCircleIcon,
} from "@heroicons/react/24/solid";
import {
  HomeIcon as HomeIconOutline,
  StarIcon as StarIconOutline,
  SunIcon as SunIconOutline,
  UserCircleIcon as UserCircleIconOutline,
  UserGroupIcon,
} from "@heroicons/react/24/outline";

export const Sidebar = () => {
  return (
    <aside className="sidebar">
      <header className="desktop-header p-6">
        <div className="content flex justify-between">
          <div className="image-container h-6 w-20 relative ">
            <img src="/images/asana-logo.svg" alt="" />
          </div>
          <Bars3Icon className="w-6 bars3-icon" />
        </div>
      </header>
      <nav className="desktop-nav pt-3">
        <ul className="list">
          <li className="item px-6 active">
            <a href="#" className="link flex">
              <HomeIconOutline className="w-5 mr-2" />
              Home
            </a>
          </li>
          <li className="item px-6">
            <a href="#" className="link flex">
              <SunIconOutline className="w-5 mr-2" />
              My day
            </a>
          </li>

          <li className="item px-6">
            <a href="#" className="link flex">
              <UserCircleIconOutline className="w-5 mr-2" />
              Profile
            </a>
          </li>
        </ul>
      </nav>
    </aside>
  );
};
