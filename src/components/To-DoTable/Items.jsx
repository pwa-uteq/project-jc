import { CheckCircleIcon, StarIcon, XCircleIcon } from "@heroicons/react/24/outline";
import { useDispatch, useSelector } from "react-redux";
import { deleteTask, updateTask, favoriteTask } from "../../store/slices/rootSlice";

const Items = ({ task }) => {
  const dispatch = useDispatch();

  const notification = () => {
    if (!window.Notification) {
      console.log('Este navegador soporta notificaciones')
      return
    }
    if (Notification.permission === 'granted') {
      new Notification('Has terminado la tarea con éxito')
    }
    else if (Notification.permission !== 'denied' || Notification.permission === 'default') {
      Notification.requestPermission((permission) => {
        console.log(permission);
        if (permission === 'granted') {
          new Notification('Has terminado la tarea con éxito')
        }
      })
    }
  }
  
  const handleDone = () => {
    const updatedTask = { ...task, isDone: true };
    dispatch(updateTask(updatedTask));
    notification()
  };
  const handleFavorite = (status) => {
    if(status !== false){
      const updatedTask = { ...task, favorite: false };
      dispatch(updateTask(updatedTask));
    }else{
      const updatedTask = { ...task, favorite: true };
      dispatch(updateTask(updatedTask));
    }
    
  };

  return (
    <li className="todo-item py-2">
      <div className="item-content flex justify-between">
        <div className="right-content flex items-center">
          <CheckCircleIcon
            onClick={handleDone}
            className="w-5 mr-2 check-icon cursor-pointer"
          />
          <span className="text small"> {task.title}</span>
        </div>
        <XCircleIcon
          onClick={() => dispatch(deleteTask(task.id))}
          className="w-5 mr-2 delete-icon cursor-pointer"
        />
        <StarIcon className="w-5 mr-2 delete-icon cursor-pointer" onClick={() => handleFavorite((task.favorite))} />
      </div>
    </li>
  );
};

export default Items;
