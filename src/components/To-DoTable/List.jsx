import React from "react";
import Items from "./Items";
import { useSelector } from "react-redux";

export const List = () => {
  var tasks = useSelector((state) => state.searchTask);
  const tasksFavorite = useSelector((state) => state.favoriteTask);
  const filtreFavorite = useSelector((state) => state.favorite);

  if(filtreFavorite === true){
    tasks = tasksFavorite
  }
  return (
    <ul className="todo-list">
      {tasks
        .filter((task) => task.isDone === false)
        .map((task) => (
          <Items key={task.id} task={task} />
        ))}
    </ul>
  );
};
