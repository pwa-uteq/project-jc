import { List } from "./List";

export const Table = ({}) => {
  return (
    <div className="todo-table px-8 py-6 rounded-lg">
      <div className="container">
        <div className="header flex items-center pb-4">
          <div className="image-container w-10 h-10 relative mr-2">
            <img src="/images/josh-profile.png" alt="" />
          </div>
          <span className="text normal-semibold">My tasks</span>
        </div>
        <div className="body">
          <List />
        </div>
      </div>
    </div>
  );
};
