import { PlusIcon, StarIcon } from "@heroicons/react/24/solid";
import { useDispatch } from "react-redux";
import {
  setIsFormOpen,
  filterTask,
  favoriteTask,
} from "../store/slices/rootSlice";

export const Topbar = ({ title }) => {
  const dispatch = useDispatch();

  const search = (event) => {
    dispatch(filterTask(event.target.value));
  };

  return (
    // Esto es un comentario de prueba
    <div className="topbar items-center flex w-full">
      <div className="topbar-container flex w-full justify-between px-4 lg:px-8">
        <h4 className="h4">{title}</h4>

        <div className="left-content gap-4 flex items-center">
          <input
            className="search-input pl-8 "
            placeholder="Search"
            type="search"
            autoComplete="off"
            onChange={search}
          />
          <div className="buttons flex gap-4">
            <div
              onClick={() => dispatch(setIsFormOpen(true))}
              className="plus-button p-2 rounded-full  cursor-pointer"
            >
              <PlusIcon className="w-4 h-4 plus-icon" />
            </div>
            <div className="plus-button p-2 rounded-full  cursor-pointer">
              <StarIcon
                className="w-4 h-4 plus-icon"
                onClick={() => dispatch(favoriteTask(true))}
              />
            </div>
          </div>

          <div className="profile flex items-center rounded-full px-3">
            <div className="image-container w-6 h-6 relative mr-2">
              <img src="/images/josh-profile.png" alt="" />
            </div>
            <span className="profile-text">Josh G</span>
          </div>
        </div>
      </div>
    </div>
  );
};
