import { createSlice } from "@reduxjs/toolkit";

const rootSlice = createSlice({
  name: "root",
  initialState: {
    isFormOpen: false,
    favorite: false,
    tasks: [],
    searchTask: [],
    favoriteTask: [],
  },
  reducers: {
    setIsFormOpen: (state, { payload }) => {
      state.isFormOpen = payload;
      return state;
    },
    addTask: (state, { payload }) => {
      state.tasks = [...state.tasks, payload];
      state.searchTask = state.tasks;
      return state;
    },
    updateTask: (state, { payload }) => {
      const prevTask = state.tasks.filter((task) => task.id !== payload.id);
      state.tasks = [...prevTask, payload];
      state.searchTask = state.tasks;
      state.favoriteTask = state.tasks.filter(
        (task) => task.favorite !== false
      );
      return state;
    },
    clearTasks: (state) => {
      state.tasks = [];
      state.searchTask = [];
      state.favoriteTask = [];
      return state;
    },
    deleteTask: (state, { payload }) => {
      state.tasks = state.tasks.filter((task) => task.id !== payload);
      state.searchTask = state.tasks;

      return state;
    },
    filterTask: (state, { payload }) => {
      if (!payload.length >= 1) {
        state.searchTask = state.tasks;
      } else {
        state.searchTask = state.tasks.filter((task) =>
          task.title.toLowerCase().includes(payload)
        );
      }
      return state;
    },
    favoriteTask: (state, { payload }) => {
      state.favoriteTask = state.tasks.filter(
        (task) => task.favorite !== false
      );
      if (state.favorite !== payload) {
        state.favorite = payload;
      } else {
        state.favorite = false;
      }
      return state;
    },
  },
});
export const {
  setIsFormOpen,
  addTask,
  updateTask,
  clearTasks,
  deleteTask,
  filterTask,
  favoriteTask,
} = rootSlice.actions;
export default rootSlice.reducer;
